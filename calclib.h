#include <stdio.h>


int add() {


    int num1;
    int num2;
    int result;

    printf("Enter an integer:");
    scanf(" %d", &num1);

    printf("Enter a second integer:");
    scanf(" %d", &num2);

    result = num1 + num2;

    printf("The result is: %d\n", result);
    return result;

}

int subtract() {


    int num1;
    int num2;
    int result;

    printf("Enter the first integer:");
    scanf(" %d", &num1);

    printf("Enter the second integer to subtract from the first:");
    scanf(" %d", &num2);

    result = num1 - num2;

    printf("The result is: %d\n", result);
    return result;

}


int divide() {


    int num1;
    int num2;
    int result;

    printf("Enter the first integer:");
    scanf(" %d", &num1);

    printf("Enter the second integer to divide the first by:");
    scanf(" %d", &num2);

    result = num1 / num2;

    printf("The result is: %d\n", result);
    return result;

}


int multiply() {


    int num1;
    int num2;
    int result;

    printf("Enter an integer:");
    scanf(" %d", &num1);

    printf("Enter a second integer:");
    scanf(" %d", &num2);

    result = num1 * num2;

    printf("The result is: %d\n", result);
    return result;

}
