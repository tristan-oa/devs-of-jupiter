#include <stdio.h>
#include "calclib.h"


typedef enum { false, true } bool;

bool fivePressed = false;

    int exitCalc(){

        fivePressed = true;
        printf("Thanks for using c.a.l.c!\n");
        return 0;

    }

    int main(){



        while(fivePressed == false){

            int selection;

            printf("Welcome to  c.a.l.c, arguably the most crude calculator you will ever make use of :)\n");
            printf("-------------------------\n");
            printf("1 - Add X and Y\n");
            printf("2 - Subtract X from Y\n");
            printf("3 - Divide X by Y\n");
            printf("4 - Multiply X by Y\n");
            printf("5 - Quit c.a.l.c.\n");
            printf("-------------------------\n");
            printf("Which one will you choose?");
            scanf("%d", &selection);




            switch (selection)

            {

                case 1:
                    add();
                    break;


                case 2:
                    subtract();
                    break;


                case 3:
                    divide();
                    break;

                case 4:
                    multiply();
                    break;

                case 5:
                    exitCalc();
                    break;

                default:
                    printf("To get started, please enter a number from 1 to 5.\n");

            }



            return 0;

        }
    }
